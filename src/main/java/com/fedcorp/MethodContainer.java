package com.fedcorp;
@FunctionalInterface
public interface MethodContainer {
    void run();
}
