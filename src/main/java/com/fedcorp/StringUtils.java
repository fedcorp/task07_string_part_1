package com.fedcorp;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {
    private List<Object> objects = new ArrayList<Object>();

    public StringUtils addToParameters(Object obj){
        objects.add(obj);
        return this;
    }

    public String concat(){
        StringBuilder stringBuilder = new StringBuilder();
        for (Object obj : objects){
            stringBuilder.append(obj).append("  ");
        }
        return stringBuilder.toString();
    }
}