package com.fedcorp;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyView {

    private Map<String, String> languageMenuView;
    private Map<String, String> methodsMenuView;
    private Map<String, MethodContainer> languageMenu;
    private Map<String, MethodContainer> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    Locale locale;
    ResourceBundle bundle;

    public MyView() {

        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("l18n", locale);

        setLanguageMenu();
        setMethodsMenu();

        languageMenu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        languageMenu.put("1", this::setEnglish);
        languageMenu.put("2", this::setFrench);
        languageMenu.put("3", this::setSpain);
        languageMenu.put("4", this::setUkrainian);

        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::testRegEx);
        methodsMenu.put("3", this::test3);
        methodsMenu.put("B", this::show);
    }

    private void setLanguageMenu() {

        languageMenuView = new LinkedHashMap<>();

        languageMenuView.put("1", bundle.getString("1"));
        languageMenuView.put("2", bundle.getString("2"));
        languageMenuView.put("3", bundle.getString("3"));
        languageMenuView.put("4", bundle.getString("4"));
        languageMenuView.put("Q", bundle.getString("Q"));
    }

    private void setMethodsMenu() {

        methodsMenuView = new LinkedHashMap<>();

        methodsMenuView.put("1", bundle.getString("5"));
        methodsMenuView.put("2", bundle.getString("6"));
        methodsMenuView.put("3", bundle.getString("7"));
        methodsMenuView.put("B", bundle.getString("BECK"));
        methodsMenuView.put("Q", bundle.getString("Q"));
    }

    private void setUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("l18n", locale);
        setMethodsMenu();
        setLanguageMenu();
        showMethods();
    }

    private void setEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("l18n", locale);
        setMethodsMenu();
        setLanguageMenu();
        showMethods();
    }

    private void setFrench() {
        locale = new Locale("fr");
        bundle = ResourceBundle.getBundle("l18n", locale);
        setMethodsMenu();
        setLanguageMenu();
        showMethods();
    }

    private void setSpain() {
        locale = new Locale("es");
        bundle = ResourceBundle.getBundle("l18n", locale);
        setMethodsMenu();
        setLanguageMenu();
        showMethods();
    }

    private void testStringUtils() {
        StringUtils utils = new StringUtils();
        utils.addToParameters(11)
                .addToParameters(22.05)
                .addToParameters("Fedoriv");
        System.out.println(utils.concat());
    }

    private void testRegEx() {
        String message = "Hello, I am Oleh";
        System.out.println(message);
        String message2 = message.replaceAll("e", "_");
        System.out.println(message2);
        String[] mm = message.split("e");
        for (String str : mm) {
            System.out.println(str);
        }
        System.out.println(message.matches("Hello, I am Oleh"));

        Pattern pattern = Pattern.compile("^Hello%");
        Matcher matcher = pattern.matcher(message);
        System.out.println(matcher.matches());
    }

    private void test3() {

        String s1 = "Bat";
        String s2 = "Bat";
        String s3 = new String("Bat");
        String s4 = s3.intern();

        System.out.println("s1 == s2 :"+(s1==s2));
        System.out.println("s1 == s3 :"+(s1==s3));

        System.out.println(System.identityHashCode(s1));
        System.out.println(System.identityHashCode(s2));
        System.out.println(System.identityHashCode(s3));
        System.out.println(System.identityHashCode(s4));
    }

    //-------------------------------------------------------------------------

    private void showLanguageMenu() {
        System.out.println("\nMENU:");
        for (String key : languageMenuView.keySet()) {
            if (key.length() == 1) {
                System.out.println(languageMenuView.get(key));
            }
        }
    }

    private void showMethodsMenu() {
        System.out.println("\nMENU:");
        for (String key : methodsMenuView.keySet()) {
            if (key.length() == 1) {
                System.out.println(methodsMenuView.get(key));
            }
        }
    }
// Потрібно позбутися циклів do-while щоб виправити проблему виходу з програми не з першого разу.
    public void show() {
        String keyMenu;
        do {
            showLanguageMenu();
            System.out.println(bundle.getString("SELECT"));
            keyMenu = input.nextLine().toUpperCase();
            if (!keyMenu.equals("Q")) {
                try {
                    languageMenu.get(keyMenu).run();
                } catch (Exception e) {
                }
            }else break;
        }while(!keyMenu.equals("Q")|!keyMenu.equals("1")|!keyMenu.equals("2")|!keyMenu.equals("3")|!keyMenu.equals("4"));
    }

    public void showMethods() {
        String keyMenu;
        do {
            showMethodsMenu();
            System.out.println(bundle.getString("SELECT"));
            keyMenu = input.nextLine().toUpperCase();
            if (keyMenu.equals("Q")) break;
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
